<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WeatherMood</title>
    <link rel="icon" type="image/png" href="image/ciel1.jpeg"/>
    <!-- CSS BOOTSTRAP -->
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <!-- WEATHER ICONS -->
    <link rel="stylesheet" href="weather-icons-master/css/weather-icons.min.css">
    <!-- FONT PERSO -->
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <!-- CSS PERSO-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Jquery JS-->
    <script src="jquery-3.1.1.min.js"></script>
    <!-- BOOTSTRAP JS -->
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <!-- CUSTOM JS -->
    <script src="custom.js"></script>
</head>
<body>
<?php
const URL_API_WEATHER = 'https://api.openweathermap.org/data/2.5/forecast?q=';
const API_KEY_WEATHER = '&appid=6c3ae00bfde9d0251218bf15b1e16c9a';
const API_PARAM_WEATHER = '&units=metric&lang=fr';
//const URL_DATA_API_WEATHER = 'http://api.openweathermap.org/data/2.5/forecast?id=';
const URL_API_DEEZER = 'https://api.deezer.com/search?q=';
const URL_READER_DEEZER = 'https://www.deezer.com/plugins/player?format=square&autoplay=true&playlist=true&color=007feb&layout=dark&size=big&type=tracks&id=';

if (isset($_POST['submit'])) {
    //if submit is push without datas in the search bar, reload index.php and get message
    if (empty($_POST['search'])) {
        $msg = "Vous avez lancé une recherche sans nom de ville!";
        header('location: index.php?&emptyCity=1');
    } else {
        //Search word
        $post = trim(htmlentities(str_replace(' ', '', ucfirst($_POST['search']))));
        //Get datas from http request to openweathermap
        $jsonCurrentWeahter = file_get_contents(URL_API_WEATHER . $post . API_KEY_WEATHER . API_PARAM_WEATHER);
        if (!$jsonCurrentWeahter) {
            //if submit nothing corresponding from the cities list, reload index.php and get message
            header('location: index.php?&badCity=1&search=' . $_POST['search']);
        } else {
            $currentWeather = json_decode($jsonCurrentWeahter, true);

            //Tableau pour exploiter les valeurs dans la vue
            $valeurs = [
                'ville' => $currentWeather['city']['name'],
                'pays' => $currentWeather['city']['country'],
                'conditions_actu' => $currentWeather['list'][0]['weather'][0]['main'],
                'icone' => $currentWeather['list'][0]['weather'][0]['icon'],
                'temperature' => $currentWeather['list'][0]['main']['temp'],
                'humidite' => $currentWeather['list'][0]['main']['humidity'],
                'vent' => $currentWeather['list'][0]['wind']['speed'],
                'conditions_demain' => [
                    'condition' => $currentWeather['list'][1]['weather'][0]['main'],
                    'icone' => $currentWeather['list'][1]['weather'][0]['icon'],
                    'temperature' => $currentWeather['list'][1]['main']['temp'],
                    'humidite' => $currentWeather['list'][1]['main']['humidity'],
                    'vent' => $currentWeather['list'][1]['wind']['speed'],
                ],
            ];
            //Change conditions english to french
            $conditions = [
                'Clear' => 'Dégagé',
                'Rain' => 'Pluvieux',
                'Snow' => 'Neigeux',
                'Clouds' => 'Nuageux',
                'Thunderstorm' => 'Orageux',
                'Drizzle' => 'Bruineux',
                'Atmosphere' => 'Brumeux',
            ];
            //Array to change openweather icon to weather icon from toolkit => https://erikflowers.github.io/weather-icons/
            $icons = ['01d' => 'wi-day-sunny',
                '02d' => 'wi-day-cloudy',
                '03d' => 'wi-cloud',
                '04d' => 'wi-cloudy',
                '09d' => 'wi-rain',
                '10d' => 'wi-day-showers',
                '11d' => 'wi-thunderstorm',
                '13d' => 'wi-snow',
                '50d' => 'wi-fog',
                '01n' => 'wi-night-clear',
                '02n' => 'wi-night-alt-cloudy',
                '03n' => 'wi-cloud',
                '04n' => 'wi-cloudy',
                '09n' => 'wi-rain',
                '10n' => 'wi-night-alt-showers',
                '11n' => 'wi-thunderstorm',
                '13n' => 'wi-snow',
                '50n' => 'wi-fog',
            ];
            $sun = ['sun', 'happy', 'samba', 'sunshine', 'diamon', 'smile', 'reagge', 'ragga', 'good vibes', 'fun', 'nice', 'love'];
            $rain = ['sad', 'homeless', 'alone', 'bad', 'hangry', 'dark', 'moon', 'cloud', 'rain', 'sickness'];
            $drizzle = ['sad', 'homeless', 'alone', 'bad', 'hangry', 'dark', 'moon', 'cloud', 'rain', 'sickness'];
            $snow = ['cold', 'snow', 'christmas', 'chirstmas tree', 'wind', 'winter', 'fireplace'];
            $cloud = ['Clouds', 'Cloud', 'Cloudy', '', '', '', '', '', '', '', '', '', ''];
            $atmosphere = ['thunder', 'strom', 'thunderstorm', 'cyclone', 'mist', 'sad day', 'black mind', 'emergency'];

            $keyWord = [
                'Clear' => $sun[array_rand($sun)],
                'Rain' => $rain[array_rand($rain)],
                'Drizzle' => $drizzle[array_rand($drizzle)],
                'Snow' => $snow[array_rand($snow)],
                'Clouds' => $cloud[array_rand($cloud)],
                'Atmosphere' => $atmosphere[array_rand($atmosphere)],
            ];
            //Get datas from http request to deezer with keyword in parameter
            $dzReturn = file_get_contents(URL_API_DEEZER . '"' . $keyWord[$valeurs['conditions_actu']] . '"');
            $tracks = json_decode($dzReturn, true);
            if ($tracks['total'] > 0) {
                //Random id track to play in deezer
                $track = $tracks['data'][array_rand($tracks['data'])]['id'];
            } else {
                header('location: index.php?&trackError=1&search=' . $_POST['search']);
            }
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-4">
            <div class="row meteo text-center">
                <div class="col-lg-12 tuile">
                    <?php
                    echo "
                            <div class='col-xs-12'>
                                <h1> Bienvenue à " . $valeurs['ville'] . " " . $valeurs['pays'] . "</h1>
                                <p> Nous sommes le " . date('d-m-Y') . "</p>
                            </div>
                            <div class='col-xs-12 col-lg-offset-2 col-lg-8'>
                                <form class='newForm' method='POST' action='weathermood.php'>
                                    <div class='input-group'>
                                        <input class='form-control' type='text' name='search' id='search' placeholder='Nouvelle recherche'>
                                        <span class='input-group-btn'>
                                            <input type='submit' class='btn btn-default' name='submit' id='submit' value='GO!'>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class='col-xs-12 col-lg-offset-2 col-lg-8'>
                                <a id='returnLink' class='btn btn-default btn-lg text-center' href='index.php'>
                                    Retour
                                </a>
                            </div>
                            <div class='col-xs-12'>
                                <p>
                                    Dernière mise à jour à : " . date('H') . " heure et " . date('i') . " minutes 
                                </p>
                            </div>
                            ";
                    ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-8">
            <div class="row meteo text-center">
                <div class="col-xs-12 col-lg-12 tuile">
                    <div class="col-xs-12">
                        <h1>Conditions Actuelles</h1>
                    </div>
                    <div class="col-xs-6 col-lg-3 hover">
                        <?php
                        echo '<h3>Temps ' . $conditions[$valeurs['conditions_actu']] . '</h3>
							      <i class="iconTop wi ' . $icons[$valeurs['icone']] . '"></i>
							';
                        ?>
                    </div>
                    <div class="col-xs-6 col-lg-3 hover">
                        <?php
                        echo '<h3>Température ' . $valeurs['temperature'] . ' °C</h3>';
                        if ($valeurs['temperature'] < 10) {
                            echo '<i class="iconTop wi wi-thermometer-exterior"></i>';
                        } else {
                            echo '<i class="iconTop wi wi-thermometer"></i>';
                        }
                        ?>
                    </div>
                    <div class="col-xs-6 col-lg-3 hover">
                        <?php
                        echo '<h3>Humidité ' . $valeurs['humidite'] . ' %</h3>
                                  <i class="iconTop wi wi-humidity"></i>';
                        ?>
                    </div>
                    <div class="col-xs-6 col-lg-3 hover">
                        <?php
                        echo '<h3>Vent ' . $valeurs['vent'] . ' km/h</h3>
                                  <i class="iconTop wi wi-strong-wind"></i>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="sideCol" class="col-xs-12 col-lg-4">
            <div class="wrapCol col-xs-12  col-lg-12">
                <div class="row meteo text-center">
                    <div class="col-xs-12 col-lg-12 tuile">
                        <div class="col-xs-12">
                            <h1>Les conditions pour demain</h1>
                        </div>
                        <?php
                        echo '
                            <div class="col-xs-6 col-lg-6 hover">
                                <i class="iconSide wi ' . $icons[$valeurs['conditions_demain']['icone']] . '"></i>
                                ' . $conditions[$valeurs['conditions_demain']['condition']] . '
                            </div>
                            <div class="col-xs-6 col-lg-6 hover">
                                <i class="iconSide wi wi-thermometer"></i>
                                ' . $valeurs['conditions_demain']['temperature'] . '°C
                            </div>
                            <div class="col-xs-6 col-lg-6 hover">
                                <i class="iconSide wi wi-humidity"></i>
                                '. $valeurs['conditions_demain']['humidite'] . '%
                            </div>
                            <div class="col-xs-6 col-lg-6 hover">
                                <i class="iconSide wi wi-strong-wind"></i>
                                '. $valeurs['conditions_demain']['vent'] . ' km/h
                            </div>';
                        ?>
                    </div>
                </div>
            </div>
            <div class="wrapCol col-xs-12  col-lg-12">
                <div class="row meteo text-center">
                    <div class="col-lg-12 tuile">
                        <?php
                        if (isset($track)) {
                            //Video playlist for background
                            $playlist = ['Clear' => 'clear.mp4',
                                'Rain' => 'rain.mp4',
                                'Drizzle' => 'rain.mp4',
                                'Snow' => 'snow.mp4',
                                'Clouds' => 'clouds.mp4',
                                'Atmosphere' => 'atmosphere.mp4'];
                            // New version of deezer player
                            $readerLink = URL_READER_DEEZER . $track . '&app_id=1';
                            echo '<iframe class="music" scrolling="no" allowTransparency="true"  src="' . $readerLink . '"></iframe>';
                            //Background video
                            echo '
                                <video class="bgvid" playsinline autoplay muted loop>
                                    <source src="image/' . $playlist[$currentWeather['list'][0]['weather'][0]['main']] . '" type="video/mp4">
                                </video>
                                ';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="mapCol" class="col-xs-12 col-lg-8">
            <div class="row meteo text-center">
                <div class="col-lg-12 tuile">
                    <?php
                    $mapLink = 'https://maps.darksky.net/@precipitation_rate,' . $currentWeather['city']['coord']['lat'] . ',' . $currentWeather['city']['coord']['lon'] . ',5?embed=true&timeControl=false&fieldControl=false&defaultField=precipitation_rate';
                    echo '<iframe class="map" src="' . $mapLink . '" frameborder="0"></iframe>';
                    ?>
                    <a class="darkskyLink" href="https://darksky.net/poweredby/">Powered by Dark Sky</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>