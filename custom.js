$(function () {
    var $window = $(window);
    var $sideCol = $('#sideCol');
    var $mapCol = $('#mapCol');
    var $map = $('.map');
    var $music = $('.music');
    var $marginAndPaddingSideCol = Number($sideCol.css('padding-top').replace('px',''))+
        Number($sideCol.css('padding-bottom').replace('px',''))+
        Number($sideCol.css('margin-top').replace('px',''))+
        Number($sideCol.css('margin-bottom').replace('px',''));
    $window.on('load resize', function () {
        $mapCol.height($sideCol.height() - $marginAndPaddingSideCol);
        $map.height($mapCol.height()-75);
        $music.height($music.parent('div').height());
    })

});