<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WeatherMood</title>
    <link rel="icon" type="image/png" href="image/ciel1.jpeg"/>
    <!-- CSS BOOTSTRAP -->
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <!-- CSS PERSO-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- FONT PERSO -->
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <!-- Jquery JS-->
    <script src="jquery-3.1.1.min.js"></script>
    <!-- JqueryUI JS-->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <!-- BOOTSTRAPP JS -->
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
<div id="homePage" class="container-fluid text-center">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="titleSearch">WeatherMood Search!</h1>
            <h3>Stream your weather...</h3>
        </div>
        <div class="col-xs-12 col-lg-offset-3 col-lg-6">
            <form id="formulaire" method="POST" action="weathermood.php">
                <div class="input-group">
                    <input class="form-control" type="text" name="search" id="search" placeholder="Saisissez le nom d'une ville">
                    <span class="input-group-btn">
                        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="GO!">
                    </span>
                </div>
            </form>
        </div>
        <div class="col-xs-12">
            <?php
            if (isset($_GET['emptyCity'])) {
                if ($_GET['emptyCity'] == 1) {
                    echo '<h1>Vous n\'avez rien entré dans le champ recherche.</h1>';
                }
            }
            if (isset($_GET['badCity'])) {
                if ($_GET['badCity'] == 1) {
                    echo '<h1>Aucun résultat pour la ville "' . $_GET['search'] . '"</h1>';
                }
            }
            if (isset($_GET['trackError'])) {
                if ($_GET['trackError'] == 1) {
                    echo '<h1>Aucune chanson de trouvée pour la météo de la ville "' . $_GET['search'] . '"</h1>';
                }
            }
            ?>
        </div>
    </div>
</div>
<video class="bgvid" playsinline autoplay muted loop poster="image/ciel.jpeg">
    <source src="image/sky.mp4" type="video/mp4">
</video>
</body>
</html>
